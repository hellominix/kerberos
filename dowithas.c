#include	"kerberos.h"
#include 	"des.h"

void
dowithas(int sockfd,char*ticketoftgs,char *keyofctgs)
{
	char keyofclient[KEYLENGTH]=
{0x45,0xec,0xbb,0x42,0x84,0xd3,0x82,0xad};
	
	char idc[IDLENGTH]="idc";
	
	char idtgs[IDLENGTH]="idtgs";
	
	char timestamp[TSLENGTH];
	
	snprintf(timestamp,TSLENGTH,"%ld",time(NULL));
	
	char* client_to_as=(char*)malloc(sizeof(char)*1024);

	memcpy(client_to_as,idc,IDLENGTH);
	memcpy(client_to_as+IDLENGTH,idtgs,IDLENGTH);
	memcpy(client_to_as+2*IDLENGTH,timestamp,TSLENGTH);
	
	int client_to_as_length=2*IDLENGTH+TSLENGTH;
	
	fprintf(stderr,"client_to_as:\n");
	printHex(client_to_as,client_to_as_length);
	
	write(sockfd, client_to_as, client_to_as_length);
	
	char	*recvline=(char*)malloc(sizeof(char)*MAXLINE);
	
	if (read(sockfd, recvline, MAXLINE) == 0)
	perror("str_cli: server terminated prematurely");
	
	int recvlength=3*IDLENGTH+2*(TSLENGTH+LIFETIMELENGTH+KEYLENGTH)+ADDRLENGTH;
	
	fprintf(stderr,"receive message:\n");
	
	printHex(recvline,recvlength);
		
	char str3[1024] = {0};
	Des_SetKey(keyofclient);
        Des_de(recvline,str3,recvlength);
	fprintf(stderr,"AS to client M message\n");
	printHex(str3,recvlength);

	int ticketlength=2*IDLENGTH+KEYLENGTH+ADDRLENGTH+TSLENGTH+LIFETIMELENGTH;
	int i;
	for(i=0;i<ticketlength;i++)
	{
	 ticketoftgs[i]=str3[i+recvlength-ticketlength];
	}
	fprintf(stderr,"ticket of tgs message:\n");
	printHex(ticketoftgs,ticketlength);
	
	for(i=0;i<KEYLENGTH;i++)
	{
	 keyofctgs[i]=str3[i];
	}

}

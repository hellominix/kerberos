#include	"kerberos.h"
#include        "des.h"

void
str_echo(int sockfd)
{
	int authenticatorv_length=IDLENGTH+ADDRLENGTH+TSLENGTH;
	
	int ticketv_length=KEYLENGTH+TSLENGTH+2*IDLENGTH+ADDRLENGTH+LIFETIMELENGTH;
	
	int client_to_v_length=ticketv_length+authenticatorv_length;
	
	char buf[MAXLINE];
	
	while ( (read(sockfd, buf,client_to_v_length)) != client_to_v_length)	
	perror("str_echo: read error");
	
	fprintf(stderr,"client to v:\n\n");
	printHex(buf,client_to_v_length);
	
	char en_ticket_of_v[1024]={0};
	char ticket_of_v[1024]={0};
	
	memcpy(en_ticket_of_v,buf,ticketv_length);
	
	
	char keyofv[KEYLENGTH]={0x83,0xdd,0x6e,0x9f,0xde,0xf4,0xfd,0x65};
	
	Des_SetKey(keyofv);

	Des_de(en_ticket_of_v,ticket_of_v,ticketv_length);
	fprintf(stderr,"ticket_of_v:\n\n");
	printHex(ticket_of_v,ticketv_length);
	
	char keyofcv[KEYLENGTH];
	memcpy(keyofcv,ticket_of_v,KEYLENGTH);
	fprintf(stderr,"keyofcv:\n\n");
	printHex(keyofcv,KEYLENGTH);
	
        char en_authenticator_v[1024]={0};
        char authenticator_v[1024]={0};
        memcpy(en_authenticator_v,buf+ticketv_length,authenticatorv_length);
        
        Des_SetKey(keyofcv);

	Des_de(en_authenticator_v,authenticator_v,authenticatorv_length);
	fprintf(stderr,"authenticator_v:\n\n");
	printHex(authenticator_v,authenticatorv_length);
        
	char idc_from_ticket[IDLENGTH];
	char idc_from_authenticator_v[IDLENGTH];
	
	memcpy(idc_from_ticket,ticket_of_v+KEYLENGTH,IDLENGTH);
	memcpy(idc_from_authenticator_v,authenticator_v,IDLENGTH);
	if(memcmp(idc_from_ticket,idc_from_authenticator_v,IDLENGTH)!=0)
	{
	 fprintf(stderr,"can not through verificationn\n");
	 exit(1);
	}
	char adc_from_ticket[ADDRLENGTH];
	char adc_from_authenticator_v[ADDRLENGTH];
	
	memcpy(adc_from_ticket,ticket_of_v+KEYLENGTH+IDLENGTH,ADDRLENGTH);
	memcpy(adc_from_authenticator_v,authenticator_v+IDLENGTH,ADDRLENGTH);
	
	if(memcmp(adc_from_ticket,adc_from_authenticator_v,ADDRLENGTH)!=0)
	{
	 fprintf(stderr,"can not through verificationn\n");
	 exit(1);
	}
	
	char timestamp[TSLENGTH];
	snprintf(timestamp,TSLENGTH,"%ld",time(NULL)+1);

	
        char *vtoc=(char*)malloc(sizeof(char)*1024);
       
        memcpy(vtoc,timestamp,TSLENGTH);	
        fprintf(stderr,"Original message:\n\n");
	printHex(vtoc,TSLENGTH);
       
	char en_vtoc[1024] = {0};
	char str3[1024] = {0};
	
	Des_SetKey(keyofcv);

	Des_en(vtoc,en_vtoc,TSLENGTH);
		
	fprintf(stderr,"en_vtoc:\n\n");
	printHex(en_vtoc,TSLENGTH);
	
	Des_de(en_vtoc,str3,TSLENGTH);
	fprintf(stderr,"Original message:\n\n");
	printHex(str3,TSLENGTH);
	
	write(sockfd, en_vtoc, TSLENGTH);
	

}

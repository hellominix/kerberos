
PROGS =	client  as  tgs v

all:	${PROGS}

client: des.o client.o dowithas.o dowithtgs.o dowithv.o
	gcc -g -Wall -o client des.o client.o dowithas.o dowithtgs.o dowithv.o
	 

as:des.o as.o sigchldwaitpid.o astoclient.o 
	gcc -g -Wall -o $@ des.o as.o sigchldwaitpid.o  astoclient.o

tgs:des.o tgs.o sigchldwaitpid.o tgstoclient.o 
	gcc -g -Wall -o $@ des.o tgs.o sigchldwaitpid.o tgstoclient.o
	
v:des.o v.o sigchldwaitpid.o vtoclient.o 
	gcc -g -Wall -o $@ des.o v.o   sigchldwaitpid.o vtoclient.o
	

clean:
	rm -f ${PROGS} *.o

#include	"kerberos.h"
#include 	"des.h"

void dowithv(int sockfd,char *ticketv,char *keyofcv)
{	
	char *client_to_v=(char*)malloc(sizeof(char)*1024);
	
	char idc[IDLENGTH]="idc";
	
	struct sockaddr_in cliaddr;
	int cliaddrlen=sizeof(cliaddr);
	
	getsockname(sockfd,(SA  *)&cliaddr,(socklen_t *)&cliaddrlen);
	
	char *adc=inet_ntoa(cliaddr.sin_addr);
	
	
	char timestamp[TSLENGTH];
	snprintf(timestamp,TSLENGTH,"%ld",time(NULL));
	
	char* authenticatorv=(char*)malloc(sizeof(char)*1024);
	
	memcpy(authenticatorv,idc,IDLENGTH);
	memcpy(authenticatorv+IDLENGTH,adc,ADDRLENGTH);
	memcpy(authenticatorv+IDLENGTH+ADDRLENGTH,timestamp,TSLENGTH);
	
	int authenticatorv_length=IDLENGTH+ADDRLENGTH+TSLENGTH;
	
	fprintf(stderr,"Original authenticator of v message:\n\n");
	printHex(authenticatorv,authenticatorv_length);
	
	char en_authenticatorv[1024] = {0};
	
	char str3[1024] = {0};
	Des_SetKey(keyofcv);
	
	Des_en(authenticatorv,en_authenticatorv,authenticatorv_length);
		
	fprintf(stderr,"after Encrypted authenticator of v message:\n\n");
	printHex(en_authenticatorv,authenticatorv_length);
	
	
	int ticketv_length=KEYLENGTH+TSLENGTH+2*IDLENGTH+ADDRLENGTH+LIFETIMELENGTH;
	
	memcpy(client_to_v,ticketv,ticketv_length);
	memcpy(client_to_v+ticketv_length,en_authenticatorv,authenticatorv_length);
	
	int client_to_v_length=ticketv_length+authenticatorv_length;
	
	fprintf(stderr,"client to v:\n\n");
	printHex(client_to_v,client_to_v_length);

	write(sockfd, client_to_v, client_to_v_length);
	
	char	*recvline=(char*)malloc(sizeof(char)*MAXLINE);
	
	if (read(sockfd, recvline, MAXLINE) == 0)
	perror("str_cli: server terminated prematurely");
	
	int recvline_length=TSLENGTH;
	fprintf(stderr,"receive message:\n\n");
	printHex(recvline,recvline_length);
	
	Des_de(recvline,str3,recvline_length);
	
	fprintf(stderr,"de receive message:\n\n");
	printHex(str3,recvline_length);
}
	


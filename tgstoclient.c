#include	"kerberos.h"
#include        "des.h"

void
str_echo(int sockfd)
{
	int ticketlength=2*IDLENGTH+KEYLENGTH+ADDRLENGTH+TSLENGTH+LIFETIMELENGTH;
	
	int authenticator_c_length=IDLENGTH+ADDRLENGTH+TSLENGTH;
	
	int client_to_tgs_length=IDLENGTH+ticketlength+authenticator_c_length;
	
	char buf[MAXLINE];
	
	while ( (read(sockfd, buf, client_to_tgs_length)) != client_to_tgs_length)	
	perror("str_echo: read error");
	
	fprintf(stderr,"client to tgs:\n\n");
	printHex(buf,client_to_tgs_length);
	
	char idv[IDLENGTH];
		
	memcpy(idv,buf,IDLENGTH);
	fprintf(stderr,"idv:\n\n");
	printHex(idv,IDLENGTH);
	
	char en_ticket_of_tgs[1024]={0};
	char ticket_of_tgs[1024]={0};
	memcpy(en_ticket_of_tgs,buf+IDLENGTH,ticketlength);
		
	char keyoftgs[KEYLENGTH]={0xaa,0xcc,0xdd,0xff,0x33,0x55,0xee,0x88};
	
	Des_SetKey(keyoftgs);
	Des_de(en_ticket_of_tgs,ticket_of_tgs,ticketlength);
	
	fprintf(stderr,"ticket_of_tgs:\n\n");
	printHex(ticket_of_tgs,ticketlength);
	
	fprintf(stderr,"en_ticket_of_tgs:\n\n");
	printHex(en_ticket_of_tgs,ticketlength);
	
	char keyofctgs[KEYLENGTH];
	memcpy(keyofctgs,ticket_of_tgs,KEYLENGTH);
	fprintf(stderr,"keyofctgs:\n\n");
	printHex(keyofctgs,KEYLENGTH);
	
	char en_authenticator_c[1024]={0};
	char authenticator_c[1024]={0};
	memcpy(en_authenticator_c,buf+IDLENGTH+ticketlength,authenticator_c_length);
	
	Des_SetKey(keyofctgs);
	Des_de(en_authenticator_c,authenticator_c,authenticator_c_length);
	fprintf(stderr,"authenticator_c:\n\n");
	printHex(authenticator_c,authenticator_c_length);
	
	char idc_from_ticket[IDLENGTH];
	char idc_from_authenticator_c[IDLENGTH];
	
	memcpy(idc_from_ticket,ticket_of_tgs+KEYLENGTH,IDLENGTH);
	memcpy(idc_from_authenticator_c,authenticator_c,IDLENGTH);
	
	
	if(memcmp(idc_from_ticket,idc_from_authenticator_c,IDLENGTH)!=0)
	{
	 fprintf(stderr,"can not through verificationn\n");
	 exit(1);
	}
	char adc_from_ticket[ADDRLENGTH];
	char adc_from_authenticator_c[ADDRLENGTH];
	
	memcpy(adc_from_ticket,ticket_of_tgs+KEYLENGTH+IDLENGTH,ADDRLENGTH);
	memcpy(adc_from_authenticator_c,authenticator_c+IDLENGTH,ADDRLENGTH);
	
	
	
	if(memcmp(adc_from_ticket,adc_from_authenticator_c,ADDRLENGTH)!=0)
	{
	 fprintf(stderr,"can not through verificationn\n");
	 exit(1);
	}
	
	char keyofcv[KEYLENGTH]={0x38,0x3d,0x2e,0xff,0xd1,0x4f,0xcd,0x43};
	
	char keyofv[KEYLENGTH]={0x83,0xdd,0x6e,0x9f,0xde,0xf4,0xfd,0x65};
	
	char idc[IDLENGTH];
	memcpy(idc,ticket_of_tgs+KEYLENGTH,IDLENGTH);
	
	char idtgs[IDLENGTH];
	memcpy(idtgs,ticket_of_tgs+KEYLENGTH+ADDRLENGTH+IDLENGTH,IDLENGTH);
	
	char lifetime[LIFETIMELENGTH]="24h";
	
	char timestamp[TSLENGTH];
	
	snprintf(timestamp,TSLENGTH,"%ld",time(NULL));
	
	
	
	struct sockaddr_in cliaddr;
	int cliaddrlen=sizeof(cliaddr);
	
	getpeername(sockfd,(SA  *)&cliaddr,(socklen_t *)&cliaddrlen);
	
	char *adc=inet_ntoa(cliaddr.sin_addr);
		
	char *ticket_of_v = (char*)malloc(sizeof(char)*1024);
	
	memcpy(ticket_of_v,keyofcv,KEYLENGTH);
	memcpy(ticket_of_v+KEYLENGTH,idc,IDLENGTH);
	memcpy(ticket_of_v+KEYLENGTH+IDLENGTH,adc,ADDRLENGTH);
	memcpy(ticket_of_v+KEYLENGTH+IDLENGTH+ADDRLENGTH,idv,IDLENGTH);
	memcpy(ticket_of_v+KEYLENGTH+IDLENGTH+ADDRLENGTH+IDLENGTH,timestamp,TSLENGTH);
	memcpy(ticket_of_v+KEYLENGTH+IDLENGTH+ADDRLENGTH+IDLENGTH+TSLENGTH,lifetime,LIFETIMELENGTH);
	
	int ticketv_length=KEYLENGTH+TSLENGTH+2*IDLENGTH+ADDRLENGTH+LIFETIMELENGTH;
	
	fprintf(stderr,"Original ticket_of_v message:\n\n");
	printHex(ticket_of_v,ticketv_length);
	
	char en_ticket_of_v[1024] = {0};
	char str3[1024] = {0};
	
	Des_SetKey(keyofv);
	Des_en(ticket_of_v,en_ticket_of_v,ticketv_length);
	
	fprintf(stderr,"en ticket_of_v message:\n\n");
	printHex(en_ticket_of_v,ticketv_length);
	
	
       char *tgstoc=(char*)malloc(sizeof(char)*1024);
             
       
         memcpy(tgstoc,keyofcv,KEYLENGTH);
         memcpy(tgstoc+KEYLENGTH,idv,IDLENGTH);
         memcpy(tgstoc+KEYLENGTH+IDLENGTH,timestamp,TSLENGTH);
         memcpy(tgstoc+KEYLENGTH+IDLENGTH
+TSLENGTH,en_ticket_of_v,ticketv_length);

       int tgstoc_length=KEYLENGTH+IDLENGTH
+TSLENGTH+ticketv_length;
     
        fprintf(stderr,"tgs to client:\n\n");
       	printHex(tgstoc,tgstoc_length);
	
	char en_tgstoc[1024] = {0};
	
	Des_SetKey(keyofctgs);
	
	Des_en(tgstoc,en_tgstoc,tgstoc_length);
        fprintf(stderr,"en tgs to client:\n\n");
       	printHex(en_tgstoc,tgstoc_length);
      
	
	
	write(sockfd, en_tgstoc,tgstoc_length);

}

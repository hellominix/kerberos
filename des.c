#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "des.h"

#define ENCRYPT 0x01
#define DECRYPT 0x00

typedef char bool;

// 16 sub keys
static bool SubKey[16][48];

// Permuted Choice 1 (PC-1)
const static char PC1_Table[56] =
{
57, 49, 41, 33, 25, 17,   9,
   1, 58, 50, 42, 34, 26, 18,
10,   2, 59, 51, 43, 35, 27,
19, 11,   3, 60, 52, 44, 36,
63, 55, 47, 39, 31, 23, 15,
   7, 62, 54, 46, 38, 30, 22,
14,   6, 61, 53, 45, 37, 29, 
21, 13,   5, 28, 20, 12,   4
};

// Left Shifts
const static char LOOP_Table[16] =
{
1, 1, 2, 2, 2, 2, 2, 2,
1, 2, 2, 2, 2, 2, 2, 1
};

// Permuted Choice 2 (PC-2)
const static char PC2_Table[48] =
{
14, 17, 11, 24,   1,   5,
   3, 28, 15,   6, 21, 10,
23, 19, 12,   4, 26,   8,
16,   7, 27, 20, 13,   2,
41, 52, 31, 37, 47, 55,
30, 40, 51, 45, 33, 48,
44, 49, 39, 56, 34, 53,
46, 42, 50, 36, 29, 32
};

// Initial Permutation (IP)
const static char IP_Table[64] =
{
58, 50, 42, 34, 26, 18, 10, 2,
60, 52, 44, 36, 28, 20, 12, 4,
62, 54, 46, 38, 30, 22, 14, 6,
64, 56, 48, 40, 32, 24, 16, 8,
57, 49, 41, 33, 25, 17,   9, 1,
59, 51, 43, 35, 27, 19, 11, 3,
     61, 53, 45, 37, 29, 21, 13, 5,
     63, 55, 47, 39, 31, 23, 15, 7
};

// Expansion (E)
static const char E_Table[48] =
{
32,   1,   2,   3,   4,   5, 
   4,   5,   6,   7,   8,   9,
   8,   9, 10, 11, 12, 13, 
12, 13, 14, 15, 16, 17,
16, 17, 18, 19, 20, 21, 
20, 21, 22, 23, 24, 25,
24, 25, 26, 27, 28, 29, 
28, 29, 30, 31, 32,   1
};

// The (in)famous S-boxes
const static char S_Box[8][4][16] =
{
{
   // S1 
   {14,   4, 13,   1,   2, 15, 11,   8,   3, 10,   6, 12,   5,   9,   0,   7},
   { 0, 15,   7,   4, 14,   2, 13,   1, 10,   6, 12, 11,   9,   5,   3,   8},
   { 4,   1, 14,   8, 13,   6,   2, 11, 15, 12,   9,   7,   3, 10,   5,   0},
      {15, 12,   8,   2,   4,   9,   1,   7,   5, 11,   3, 14, 10,   0,   6, 13}
},
{
   // S2 
      {15,   1,   8, 14,   6, 11,   3,   4,   9,   7,   2, 13, 12,   0,   5, 10},
   { 3, 13,   4,   7, 15,   2,   8, 14, 12,   0,   1, 10,   6,   9, 11,   5},
   { 0, 14,   7, 11, 10,   4, 13,   1,   5,   8, 12,   6,   9,   3,   2, 15},
      {13,   8, 10,   1,   3, 15,   4,   2, 11,   6,   7, 12,   0,   5, 14,   9}
},
{
   // S3 
      {10,   0,   9, 14,   6,   3, 15,   5,   1, 13, 12,   7, 11,   4,   2,   8},
   {13,   7,   0,   9,   3,   4,   6, 10,   2,   8,   5, 14, 12, 11, 15,   1},
   {13,   6,   4,   9,   8, 15,   3,   0, 11,   1,   2, 12,   5, 10, 14,   7},
      { 1, 10, 13,   0,   6,   9,   8,   7,   4, 15, 14,   3, 11,   5,   2, 12}
},
{
   // S4 
      { 7, 13, 14,   3,   0,   6,   9, 10,   1,   2,   8,   5, 11, 12,   4, 15},
   {13,   8, 11,   5,   6, 15,   0,   3,   4,   7,   2, 12,   1, 10, 14,   9},
   {10,   6,   9,   0, 12, 11,   7, 13, 15,   1,   3, 14,   5,   2,   8,   4},
      { 3, 15,   0,   6, 10,   1, 13,   8,   9,   4,   5, 11, 12,   7,   2, 14}
      },
      {
   // S5 
      { 2, 12,   4,   1,   7, 10, 11,   6,   8,   5,   3, 15, 13,   0, 14,   9},
   {14, 11,   2, 12,   4,   7, 13,   1,   5,   0, 15, 10,   3,   9,   8,   6},
   { 4,   2,   1, 11, 10, 13,   7,   8, 15,   9, 12,   5,   6,   3,   0, 14},
      {11,   8, 12,   7,   1, 14,   2, 13,   6, 15,   0,   9, 10,   4,   5,   3}
      },
      {
   // S6 
      {12,   1, 10, 15,   9,   2,   6,   8,   0, 13,   3,   4, 14,   7,   5, 11},
   {10, 15,   4,   2,   7, 12,   9,   5,   6,   1, 13, 14,   0, 11,   3,   8},
   { 9, 14, 15,   5,   2,   8, 12,   3,   7,   0,   4, 10,   1, 13, 11,   6},
      { 4,   3,   2, 12,   9,   5, 15, 10, 11, 14,   1,   7,   6,   0,   8, 13}
      },
      {
   // S7 
      { 4, 11,   2, 14, 15,   0,   8, 13,   3, 12,   9,   7,   5, 10,   6,   1},
   {13,   0, 11,   7,   4,   9,   1, 10, 14,   3,   5, 12,   2, 15,   8,   6},
   { 1,   4, 11, 13, 12,   3,   7, 14, 10, 15,   6,   8,   0,   5,   9,   2},
      { 6, 11, 13,   8,   1,   4, 10,   7,   9,   5,   0, 15, 14,   2,   3, 12}
      },
      {
   // S8 
      {13,   2,   8,   4,   6, 15, 11,   1, 10,   9,   3, 14,   5,   0, 12,   7},
   { 1, 15, 13,   8, 10,   3,   7,   4, 12,   5,   6, 11,   0, 14,   9,   2},
   { 7, 11,   4,   1,   9, 12, 14,   2,   0,   6, 10, 13, 15,   3,   5,   8},
      { 2,   1, 14,   7,   4, 10,   8, 13, 15, 12,   9,   0,   3,   5,   6, 11}
      }
};

// Permutation P
const static char P_Table[32] =
{
16,   7, 20, 21, 
29, 12, 28, 17,
   1, 15, 23, 26,
   5, 18, 31, 10,
   2,   8, 24, 14,
32, 27,   3,   9,
19, 13, 30,   6,
22, 11,   4, 25
};

// Final Permutation (IP**-1)
const static char IPR_Table[64] =
{
40, 8, 48, 16, 56, 24, 64, 32,
39, 7, 47, 15, 55, 23, 63, 31,
38, 6, 46, 14, 54, 22, 62, 30,
37, 5, 45, 13, 53, 21, 61, 29,
     36, 4, 44, 12, 52, 20, 60, 28,
     35, 3, 43, 11, 51, 19, 59, 27,
34, 2, 42, 10, 50, 18, 58, 26,
33, 1, 41,   9, 49, 17, 57, 25
};

// 主函数
void Des_SetKey(const char Key[8]);
void Des_Run(char Out[8], char In[8], bool Type);

// 功能函数
static void F_func(bool In[32], const bool Ki[48]);       // f 函数
static void S_func(bool Out[32], const bool In[48]);      // S 盒代替
static void Transform(bool *Out, bool *In, const char *Table, int len); // 变换
static void Xor(bool *InA, const bool *InB, int len);      // 异或
static void RotateL(bool *In, int len, int loop);       // 循环左移
static void ByteToBit(bool *Out, const char *In, int bits);     // 字节组转换成位组
static void BitToByte(char *Out, const bool *In, int bits);     // 位组转换成字节组

// 测试用
 void printHex( char *cmd, int len );
static void printArray( const char *In, int len );

/*
	par_1 -->in
	par_2 -->out
*/
int Des_en(  char *str1 , char *str2,int length )
{
	char *p1 = str1 ;
	char *p2 = str2 ;
	
	int i=0;
	while(i<length)
	{

		Des_Run(p2, p1, ENCRYPT);
		p1 += 8 ; 
		p2 += 8 ;
		i=i+8;
		
	}
}

int Des_de(  char *str1 , char *str2,int length)
{
	char *p1 = str2 ;
	char *p2 = str1 ;
	int i=0;
	while( i<length )
	{
		Des_Run(p1, p2, DECRYPT);
		p2 += 8 ;
		p1 += 8 ;
		i=i+8;
	}
}
/*
int main(int argc, char *argv[])
{
char key[12]={1,2,3,4,5,6,7,8};
char str[]="abcdabcdabcdabcdabcdabcdabcdabcdabcdabcd123";
char str2[2048];

//printArray( PC2_Table, sizeof(PC2_Table)/sizeof(PC2_Table[0]) );

printf("Before encrypting: ");
puts(str);

Des_SetKey(key);

//memset(str2, 0, strlen(str2));
char *p1 = str ;
char *p2 = str2 ;
while( *p1 )
{
	Des_Run(p2, p1, ENCRYPT);
	p1 += 8 ; 
	p2 += 8 ;
}
p1 = str ;
p2 = str2 ;
printf("After   encrypting: ");
printHex( str2, strlen(str2) );

//memset(str, 0, strlen(str));
printf("After   decrypting: ");
while( *p2 )
{
	Des_Run(p1, p2, DECRYPT);
	p2 += 8 ;
	p1 += 8 ;
}
puts(str);

return 0;
}
*/



// == 1. Process the key ==
void Des_SetKey(const char Key[8])
{
int i;
     static bool K[64], *KL = &K[0], *KR = &K[28];
    
     ByteToBit(K, Key, 64);
    
     Transform(K, K, PC1_Table, 56);
    
     // 1.2.3 Calculate the 16 sub keys. Start with i = 1. 
     for(i=0; i<16; i++)
{
   // 1.2.3.1 Perform one or two circular left shifts on both C[i-1] and D[i-1] to get C[i] and D[i], respectively. 
         RotateL(KL, 28, LOOP_Table[i]);
         RotateL(KR, 28, LOOP_Table[i]);
        
         // 1.2.3.2 Permute the concatenation C[i]D[i] as indicated below. This will yield K[i], which is 48 bits long.
         Transform(SubKey[i], K, PC2_Table, 48);
     }
}

// == 2. Process a 64-bit data block ==
void Des_Run(char Out[8], char In[8], char Type)
{
int i;
     static bool M[64], tmp[32], *Li = &M[0], *Ri = &M[32];
    
     // 2.1 Get a 64-bit data block. If the block is shorter than 64 bits, it should be padded as appropriate for the application. 
     ByteToBit(M, In, 64);
    
     // 2.2 Perform the following permutation on the data block. 
     Transform(M, M, IP_Table, 64);
    
     //2.3 Split the block into two halves. The first 32 bits are called L[0], and the last 32 bits are called R[0]. 
    
     if( Type == ENCRYPT )
     {
      // 2.4 Apply the 16 sub keys to the data block. Start with i = 1. 
         for(i=0; i<16; i++)
         {
             memcpy(tmp, Ri, 32);
            
             // R[i] = L[i-1] xor f(R[i-1], K[i])
             F_func(Ri, SubKey[i]);
            
             // 2.4.6 Exclusive-or the resulting value with L[i-1].
             // R[I]=P XOR L[I-1]
             Xor(Ri, Li, 32);
            
             // L[i] = R[i-1]
             memcpy(Li, tmp, 32);
            
         } //2.4.8 Loop back to 2.4.1 until K[16] has been applied. 
     }
     else
     {
      // To decrypt, use the same process
   // but just use the keys K[i] in reverse order
         for(i=15; i>=0; i--)
         {
             memcpy(tmp, Li, 32);
             F_func(Li, SubKey[i]);
             Xor(Li, Ri, 32);
             memcpy(Ri, tmp, 32);
         }
}

// 2.5 Perform the following permutation on the block R[16]L[16]. (Note that block R precedes block L this time.) 
     Transform(M, M, IPR_Table, 64);
    
     BitToByte(Out, M, 64);
}

void F_func(bool In[32], const bool Ki[48])
{
     static bool MR[48];
    
     // 2.4.1 Expand the 32-bit R[i-1] into 48 bits according to the bit-selection function below. 
     Transform(MR, In, E_Table, 48);
    
     // 2.4.2 Exclusive-or E(R[i-1]) with K[i]. 
     Xor(MR, Ki, 48);
    
     // 2.4.4 Substitute the values found in the S-boxes for all B[j]. Start with j = 1. All values in the S-boxes should be considered 4 bits wide. 
     S_func(In, MR);
    
     // 2.4.5 Permute the concatenation of B[1] through B[8] as indicated below. 
     Transform(In, In, P_Table, 32);
}

void S_func(bool Out[32], const bool In[48])
{
char j,m,n;

//2.4.3 Break E(R[i-1]) xor K[i] into eight 6-bit blocks. Bits 1-6 are B[1]... 
     for(j=0; j<8; j++,In+=6,Out+=4)
     {
      // 2.4.4.1 Take the 1st and 6th bits of B[j] together as a 2-bit value (call it m) indicating the row in S[j] to look in for the substitution. 
         m = (In[0]<<1) + In[5];
        
         // 2.4.4.2 Take the 2nd through 5th bits of B[j] together as a 4-bit value (call it n) indicating the column in S[j] to find the substitution.
         n = (In[1]<<3) + (In[2]<<2) + (In[3]<<1) + In[4];
        
         // 2.4.4.3 Replace B[j] with S[j][m][n]. 
   ByteToBit(Out, &S_Box[(int)j][(int)m][(int)n], 4);
     }
}

// -- Perform the permutation --
void Transform(bool *Out, bool *In, const char *Table, int len)
{
int i;
     static bool tmp[256];
    
     for(i=0; i<len; i++)
     {
         tmp[i] = In[ Table[i]-1 ];
     }
     memcpy(Out, tmp, len);
}

// -- xor --
void Xor(bool *InA, const bool *InB, int len)
{
int i;

     for(i=0; i<len; i++)
     {
         InA[i] ^= InB[i];
     }
}

// -- Perform one or two circular left shifts --
void RotateL(bool *In, int len, int loop)
{
     static bool tmp[256];    // Sample:
             // loop=2
     memcpy(tmp, In, loop);    // In=12345678 tmp=12
     memcpy(In, In+loop, len-loop); // In=345678   
     memcpy(In+len-loop, tmp, loop); // In=34567812
}

// Sample:
// In = [0x01]
// Out = [0x01] [0x00] [0x00] [0x00] [0x00] [0x00] [0x00] [0x00]
void ByteToBit(bool *Out, const char *In, int bits)
{
    int i;

     for(i=0; i<bits; i++)
     {
      // In[i]的第N位右移N位并和0x01按位"与"运算(N=1~8)
         Out[i] = (In[i/8]>>(i%8)) & 1;
     }
}

void BitToByte(char *Out, const bool *In, int bits)
{
int i;

     memset(Out, 0, (bits+7)/8);
     for(i=0; i<bits; i++)
     {
         Out[i/8] |= In[i]<<(i%8);
     }
}

// 打印指定位置指定长度HEX值
 void printHex( char *cmd, int len )
{
int i;

for(i=0; i<len; i++)
{
   fprintf(stderr,"[%02X]",(unsigned char)cmd[i]);
}
printf("\n\n");
}

// 打印数组测试用
static void printArray( const char *In, int len )
{
int   i;
char tmp[256];

memset(tmp, 0, sizeof(tmp));

for( i=0; i<len; i++)
{
   tmp[(int)In[i]]=In[i];
}

for( i=0; i<len; i++)
{
   printf("[%02d]",(unsigned char)tmp[i]);
}
printf("\n");
}


/*
int main()
{
	char key[8] = "1234567";
	char str1[] = "I shoule be encrypted to transport";
	char str2[1024] = {0};
	char str3[1024] = {0};
	
	Des_SetKey(key);
	fprintf(stderr,"Original message: ->\n%s\n",str1);
	Des_en(str1,str2);
	
	char *p1=str1;
	char *p2 = str2;
	while( *p1 )
	{
		Des_Run(p2, p1, ENCRYPT);
		p1 += 8 ; 
		p2 += 8 ;
	}
	
	fprintf(stderr,"Encrypted ->\n%s\n",str2);	
	Des_de(str2,str3);
	fprintf(stderr,"Decrypted ->\n%s\n",str3);
}
*/



#include	"kerberos.h"
#include        "des.h"

void
str_echo(int sockfd)
{
	int client_to_as_length=2*IDLENGTH+TSLENGTH;
	
	char buf[MAXLINE];
	
	while ( (read(sockfd, buf, client_to_as_length)) != client_to_as_length)	
	perror("str_echo: read error");
	
	fprintf(stderr,"client_to_as:\n\n");
	printHex(buf,client_to_as_length);
		
	char idc[IDLENGTH];	
	memcpy(idc,buf,IDLENGTH);
	
	fprintf(stderr,"idc:\n\n");
	printHex(idc,IDLENGTH);
	
	 char idtgs[IDLENGTH]; 
	memcpy(idtgs,buf+IDLENGTH,IDLENGTH);
	fprintf(stderr,"idtgs:\n\n");
	printHex(idtgs,IDLENGTH);
	
	char timestamp1[TSLENGTH];
	memcpy(timestamp1,buf+2*IDLENGTH,TSLENGTH);
	fprintf(stderr,"client to as timestamp:\n\n");
	printHex(timestamp1,TSLENGTH);
	
	char timestamp[TSLENGTH];
	snprintf(timestamp,TSLENGTH,"%ld",time(NULL));
	 
	if(strtol(timestamp,NULL,10)-strtol(timestamp,NULL,10)>1000)
	{
	 fprintf(stderr,"the package of client to as is old");
	 exit(1);
	}
	
	char keyofctgs[KEYLENGTH]={0xe4,0x6d,0xf3,0xdc,0x32,0x7a,0x45,0x98};
	
	
	char keyoftgs[KEYLENGTH]={0xaa,0xcc,0xdd,0xff,0x33,0x55,0xee,0x88};
	
	char keyofclient[KEYLENGTH]=
{0x45,0xec,0xbb,0x42,0x84,0xd3,0x82,0xad};
	
	
	char lifetime[LIFETIMELENGTH]="24h";

	
	
	struct sockaddr_in cliaddr;
	int cliaddrlen=sizeof(cliaddr);
	
	getpeername(sockfd,(SA  *)&cliaddr,(socklen_t *)&cliaddrlen);
	
	char *adc=inet_ntoa(cliaddr.sin_addr);
		
	char *ticket_of_tgs = (char*)malloc(sizeof(char)*1024);
	
	memcpy(ticket_of_tgs,keyofctgs,KEYLENGTH);
	memcpy(ticket_of_tgs+KEYLENGTH,idc,IDLENGTH);
	memcpy(ticket_of_tgs+KEYLENGTH+IDLENGTH,adc,ADDRLENGTH);
	memcpy(ticket_of_tgs+KEYLENGTH+IDLENGTH+ADDRLENGTH,idtgs,IDLENGTH);
	memcpy(ticket_of_tgs+KEYLENGTH+IDLENGTH+ADDRLENGTH+IDLENGTH,timestamp,TSLENGTH);
	memcpy(ticket_of_tgs+KEYLENGTH+IDLENGTH+ADDRLENGTH+IDLENGTH+TSLENGTH,lifetime,LIFETIMELENGTH);
	
	int ticket_of_tgs_length=KEYLENGTH+IDLENGTH+ADDRLENGTH+IDLENGTH+TSLENGTH+LIFETIMELENGTH;
	
	fprintf(stderr,"ticket_of_tgs:\n\n");
	printHex(ticket_of_tgs,ticket_of_tgs_length);
	
	char en_ticket_of_tgs[1024] = {0};
	char str3[1024] = {0};
	
	Des_SetKey(keyoftgs);
	
	Des_en(ticket_of_tgs,en_ticket_of_tgs,ticket_of_tgs_length);
	fprintf(stderr,"en_ticket_of_tgs:\n\n");
	printHex(en_ticket_of_tgs,ticket_of_tgs_length);
	
	
       char *astoc=(char*)malloc(sizeof(char)*1024);
       
       
       	memcpy(astoc,keyofctgs,KEYLENGTH);
	memcpy(astoc+KEYLENGTH,idtgs,IDLENGTH);
	memcpy(astoc+KEYLENGTH+IDLENGTH,timestamp,TSLENGTH);
	memcpy(astoc+KEYLENGTH+IDLENGTH+TSLENGTH,lifetime,LIFETIMELENGTH);
	memcpy(astoc+KEYLENGTH+IDLENGTH+TSLENGTH+LIFETIMELENGTH,en_ticket_of_tgs,ticket_of_tgs_length);
	
	int en_astoc_length=KEYLENGTH+IDLENGTH+TSLENGTH+LIFETIMELENGTH+ticket_of_tgs_length;
	
	char en_astoc[1024] = {0};
	Des_SetKey(keyofclient);
	Des_en(astoc,en_astoc,en_astoc_length);
	
	fprintf(stderr,"en_astoc:\n\n");
	printHex(en_astoc,en_astoc_length);
	
	
	write(sockfd,en_astoc,en_astoc_length);
	
}

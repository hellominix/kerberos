#include	"kerberos.h"
#include 	"des.h"

void dowithtgs(int sockfd,char*ticketoftgs,char *ticketv,char* keyofctgs,char * keyofcv)
{
	char* client_to_tgs=(char*)malloc(sizeof(char)*1024);
	
	
	struct sockaddr_in cliaddr;
	int cliaddrlen=sizeof(cliaddr);
	
	getsockname(sockfd,(SA  *)&cliaddr,(socklen_t *)&cliaddrlen);
	
	char *adc=inet_ntoa(cliaddr.sin_addr);
	
	
	char idv[IDLENGTH]="idv";
	
	char idc[IDLENGTH]="idc";
	
	char timestamp[TSLENGTH];
	
	snprintf(timestamp,TSLENGTH,"%ld",time(NULL));
	
	char* authenticatorc=(char*)malloc(sizeof(char)*1024);
	
	memcpy(authenticatorc,idc,IDLENGTH);
	memcpy(authenticatorc+IDLENGTH,adc,ADDRLENGTH);
	memcpy(authenticatorc+IDLENGTH+ADDRLENGTH,timestamp,TSLENGTH);
	
	int authenticator_c_length=IDLENGTH+ADDRLENGTH+TSLENGTH;
	fprintf(stderr,"authenticator_c;\n\n");
	printHex(authenticatorc,authenticator_c_length);
	
	char en_authenticatorc[1024] = {0};
	
	char str3[1024] = {0};
	
	Des_SetKey(keyofctgs);
	
	Des_en(authenticatorc,en_authenticatorc,authenticator_c_length);
		
	fprintf(stderr,"en_authenticatorc:\n\n");
	printHex(en_authenticatorc,authenticator_c_length);
	
	int ticketlength=2*IDLENGTH+KEYLENGTH+ADDRLENGTH+TSLENGTH+LIFETIMELENGTH;

	memcpy(client_to_tgs,idv,IDLENGTH);
	memcpy(client_to_tgs+IDLENGTH,ticketoftgs,ticketlength);
	memcpy(client_to_tgs+IDLENGTH+ticketlength,en_authenticatorc,authenticator_c_length);
	
	
	int client_to_tgs_length=IDLENGTH+ticketlength+authenticator_c_length;
	
	fprintf(stderr,"client to tgs:\n\n");
	printHex(client_to_tgs,client_to_tgs_length);
	

	
	write(sockfd, client_to_tgs, client_to_tgs_length);


	char	*recvline=(char*)malloc(sizeof(char)*MAXLINE);
	
	if (read(sockfd, recvline, MAXLINE) == 0)
	perror("str_cli: server terminated prematurely");
	
	int recvline_length=2*(KEYLENGTH+TSLENGTH)+3*IDLENGTH+ADDRLENGTH+LIFETIMELENGTH;
	fprintf(stderr,"en_tgstoc:\n\n");
	printHex(recvline,recvline_length);
	
	Des_de(recvline,str3,recvline_length);
	
	fprintf(stderr,"tgstoc:\n\n");
	printHex(str3,recvline_length);
	
 	int ticketv_length=KEYLENGTH+TSLENGTH+2*IDLENGTH+ADDRLENGTH+LIFETIMELENGTH;
 	
 	int i;
	for(i=0;i<ticketv_length;i++)
	{
	 ticketv[i]=str3[i+recvline_length-ticketv_length];
	}
	
	
	fprintf(stderr,"ticketv:\n\n");

	printHex(ticketv,ticketv_length);
	
	for(i=0;i<KEYLENGTH;i++)
	{
	 keyofcv[i]=str3[i];
	}
	
}
	

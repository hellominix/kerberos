#ifndef	__tcp_h
#define	__tcp_h

#include	<sys/socket.h>
#include	<time.h>
#include	<sys/time.h>
#include	<netinet/in.h>
#include	<errno.h>
#include	<signal.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include        <arpa/inet.h>

#include	<unistd.h>
#include	<sys/wait.h>

#define	LISTENQ		1024
#define	MAXLINE		4096	
#define	BUFFSIZE	8192
#define	ASSERV_PORT		 9877
#define	TGSSERV_PORT		 8877
#define	VSERV_PORT		 7877

#define	SA	struct sockaddr


#define IDLENGTH 16
#define TSLENGTH 16
#define KEYLENGTH 8
#define LIFETIMELENGTH 8
#define ADDRLENGTH 24


#endif
